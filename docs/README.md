# ansible nomada

# archiso

passwd

ip addr

ip link show

# from host

create key if not exist
'''
ssh-keygen
'''

copy the key
'''
 ssh-copy-id root@192.168.X.X
'''

## inspitation

### install os
https://github.com/jsf9k/ansible-arch-install
https://github.com/antoinemartin/archlinux-ansible-install
https://github.com/badele/ansible-archlinux

### configure
https://github.com/lgaggini/ansible-arch
https://github.com/pigmonkey/spark
https://github.com/linuxpiper/ansible-arch-setup

https://github.com/jsf9k/ansible-home

# install requirements

'''
ansible-galaxy install -r requirements.yaml
'''

## start ssh server

systemctl start sshd.service



ssh-copy-id -i ~/.ssh/<your-new-id-to-install> -o 'IdentityFile ~/.ssh/<your-already-existing-id>' <servername>